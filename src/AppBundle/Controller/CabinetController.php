<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class CabinetController extends Controller
{
    /**
     * @Route("/cabinet")
     * @Security("has_role('ROLE_USER')")
     */
    public function showAction()
    {
        return $this->render('AppBundle:cabinet:shares.html.twig', ['shares' => $this->container->get('security.token_storage')->getToken()->getUser()->getShares()]);
    }
}
