<?php

namespace AppBundle\Controller\Cabinet;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class StatisticsController extends Controller
{
    /**
     * @Route("/cabinet/statistics")
     * @Security("has_role('ROLE_USER')")
     */
    public function showAction()
    {
        $dateTo   = new \DateTime();
        $dateFrom = new \DateTime();
        $dateFrom->sub(new \DateInterval('P'.$this->getParameter('statistic.period')));

        return $this->render('AppBundle:cabinet:statistics.html.twig', ['dateTo' => $dateTo->format('Y-m-d'), 'dateFrom' => $dateFrom->format('Y-m-d')]);
    }
}
