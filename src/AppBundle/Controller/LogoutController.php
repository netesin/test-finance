<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class LogoutController extends Controller
{
    /**
     * @Route("/logout")
     */
    public function logoutAction()
    {
        // Clear user token
        $this->container->get('security.token_storage')->setToken(null);

        return $this->redirectToRoute('app_default_index');
    }
}
