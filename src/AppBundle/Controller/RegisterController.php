<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Users;

class RegisterController extends Controller
{
    /**
     * @Route("/register")
     * @Method("GET")
     */
    public function showAction()
    {
        return $this->render('AppBundle::register.html.twig', ['errors' => []]);
    }

    /**
     * @Route("/register")
     * @Method("POST")
     */
    public function processAction(Request $request)
    {
        $username = $request->get('username', null);
        $password = $request->get('password', null);

        $errors     = [];
        $translator = $this->container->get('translator');
        $em         = $this->container->get('doctrine.orm.entity_manager');

        // Simple check
        if (empty($username)) {
            $errors[] = $translator->trans('pages.register.error.username_empty');
        } else {
            if ($em->getRepository('AppBundle:Users')->findOneBy(['username' => $username])) {
                $errors[] = $translator->trans('pages.register.error.username_exist');
            }
        }

        if (empty($password)) {
            $errors[] = $translator->trans('pages.register.error.password_empoty');
        }

        if (empty($errors)) {
            // if no errors do register new user
            $user = new Users();
            $user->setUsername($username);
            $user->setPassword(password_hash($password, PASSWORD_BCRYPT, ['cost' => 10]));
            $em->persist($user);
            $em->flush();

            return $this->render('AppBundle::register_success.html.twig');
        } else {
            // Need registration
            return $this->render('AppBundle::register.html.twig', ['errors' => $errors, 'username' => $username, 'password' => $password]);
        }
    }
}
