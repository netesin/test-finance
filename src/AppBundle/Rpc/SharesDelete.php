<?php

namespace AppBundle\Rpc;

use Timiki\Bundle\RpcServerBundle\Server\MethodGranted;
use Timiki\Bundle\RpcServerBundle\Method\Result;
use AppBundle\Entity\Shares;

class SharesDelete extends MethodGranted
{
    /**
     * Granted roles
     *
     * @var array
     */
    protected $granted = ['ROLE_USER'];

    /**
     * Get the method description
     *
     * @return string|null
     */
    public function getDescription()
    {
        return 'Delete shares';
    }

    /**
     * Get the method params
     *
     * @return array
     */
    public function getParams()
    {
        return [
            ['id', 'required']
        ];
    }

    /**
     * Execute the server method
     */
    public function execute(Result $result, $id)
    {
        $em    = $this->getContainer()->get('doctrine.orm.entity_manager');
        $user  = $this->getContainer()->get('security.token_storage')->getToken()->getUser();
        $share = $em->getRepository('AppBundle:Shares')->findOneBy(['id' => $id, 'user' => $user]);

        if ($share) {
            $em->remove($share);
            $em->flush();
        }

        $result->setResult(['status' => 'success']);
    }
}