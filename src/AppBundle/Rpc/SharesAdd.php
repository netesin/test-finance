<?php

namespace AppBundle\Rpc;

use Timiki\Bundle\RpcServerBundle\Server\MethodGranted;
use Timiki\Bundle\RpcServerBundle\Method\Result;
use AppBundle\Entity\Shares;

class SharesAdd extends MethodGranted
{
    /**
     * Granted roles
     *
     * @var array
     */
    protected $granted = ['ROLE_USER'];

    /**
     * Get the method description
     *
     * @return string|null
     */
    public function getDescription()
    {
        return 'Add shares';
    }

    /**
     * Get the method params
     *
     * @return array
     */
    public function getParams()
    {
        return [
            ['share', 'required'],
            ['count', 'required|integer']
        ];
    }

    /**
     * Execute the server method
     */
    public function execute(Result $result, $share, $count)
    {
        $em         = $this->getContainer()->get('doctrine.orm.entity_manager');
        $user       = $this->getContainer()->get('security.token_storage')->getToken()->getUser();
        $translator = $this->getContainer()->get('translator');
        $model      = new Shares();

        $model->setShare($share);
        $model->setCount($count);
        $model->setUser($user);

        $em->persist($model);
        $em->flush();

        $result->setResult(['status' => 'success', 'msg' => ['delete' => $translator->trans('pages.cabinet.shares.share_delete'), 'update' => $translator->trans('pages.cabinet.shares.share_update')], 'data' => ['id' => $model->getId(), 'count' => $model->getCount(), 'share' => $model->getShare()]]);
    }
}