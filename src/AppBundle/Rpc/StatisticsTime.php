<?php

namespace AppBundle\Rpc;

use Timiki\Bundle\RpcServerBundle\Server\MethodGranted;
use Timiki\Bundle\RpcServerBundle\Method\Result;
use AppBundle\Entity\Shares;
use Scheb\YahooFinanceApi\ApiClient as YahooFinanceApi;

class StatisticsTime extends MethodGranted
{
    /**
     * Granted roles
     *
     * @var array
     */
    protected $granted = ['ROLE_USER'];

    /**
     * Get the method description
     *
     * @return string|null
     */
    public function getDescription()
    {
        return 'Statistics by time';
    }

    /**
     * Get the method params
     *
     * @return array
     */
    public function getParams()
    {
        return [
            ['from', 'required'],
            ['to', 'required']
        ];
    }

    /**
     * Execute the server method
     */
    public function execute(Result $result, $from, $to)
    {
        $em           = $this->getContainer()->get('doctrine.orm.entity_manager');
        $user         = $this->getContainer()->get('security.token_storage')->getToken()->getUser();
        $sharesModels = $em->getRepository('AppBundle:Shares')->findBy(['user' => $user]);
        $client       = new YahooFinanceApi();

        $dateFrom = \DateTime::createFromFormat('Y-m-d', $from);
        $dateTo   = \DateTime::createFromFormat('Y-m-d', $to);

        // If from > to
        if ($dateFrom > $dateTo) {
            $dateFrom = \DateTime::createFromFormat('Y-m-d', $to);
            $dateTo   = \DateTime::createFromFormat('Y-m-d', $from);
            $from     = $dateFrom->format('Y-m-d');
            $to       = $dateTo->format('Y-m-d');
        }

        // If 1 day, min 7 days
        if ($dateTo->diff($dateFrom)->format('%a') < 1) {
            $dateFrom = \DateTime::createFromFormat('Y-m-d', $from);
            $dateFrom = $dateFrom->sub(new \DateInterval('P7D'));
        };

        // Check to split date by 365 days
        if ($dateTo->diff($dateFrom)->format('%a') > 365) {
            $dateFromOld = \DateTime::createFromFormat('Y-m-d', $from);
            $dateFrom    = \DateTime::createFromFormat('Y-m-d', $to);
            $dateFrom    = $dateFrom->sub(new \DateInterval('P365D'));
        } else {
            $dateFromOld = null;
        }

        // get users shares list
        $shares = [];
        foreach ($sharesModels as $sharesModel) {
            if (!array_key_exists($sharesModel->getShare(), $shares)) {
                $shares[$sharesModel->getShare()] = $sharesModel->getCount();
            }
        }

        // Get historical data
        $historical = [];
        foreach ($shares as $share => $count) {
            try {
                $historical[$share] = $client->getHistoricalData($share, $dateFrom, $dateTo);
            } catch (\Scheb\YahooFinanceApi\Exception\ApiException $e) {
                $historical[$share] = [];
            }
        }

        // Summ
        $values = [];
        foreach ($historical as $share => $temp) {
            if (array_key_exists('query', $temp)) {
                foreach ($temp['query']['results']['quote'] as $data) {
                    if (array_key_exists($data['Date'], $values)) {
                        $values[$data['Date']] = bcadd($values[$data['Date']], bcmul($shares[$share], $data['Adj_Close'], 4), 4);
                    } else {
                        $values[$data['Date']] = bcmul($shares[$share], $data['Adj_Close'], 4);
                    }
                }
            }
        }

        // Format
        $valuesFormat = [];
        foreach ($values as $date => $summ) {
            $timestamp      = \DateTime::createFromFormat('Y-m-d', $date);
            $valuesFormat[] = ['x' => $timestamp->format('U'), 'y' => $summ];
        }

        if ($dateFromOld !== null) {
            // Need add interval
            $nextResult = new Result();
            call_user_func_array([$this, 'execute'], [&$nextResult, $dateFromOld->format('Y-m-d'), $dateFrom->format('Y-m-d')]);
            $valuesFormat = array_merge($valuesFormat, $nextResult->getResult()['data']);
        }

        $result->setResult(['status' => 'success', 'data' => $valuesFormat]);
    }
}