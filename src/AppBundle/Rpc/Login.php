<?php

namespace AppBundle\Rpc;

use Timiki\Bundle\RpcServerBundle\Server\Method;
use Timiki\Bundle\RpcServerBundle\Method\Result;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class Login extends Method
{
    /**
     * Get the method description
     *
     * @return string|null
     */
    public function getDescription()
    {
        return 'Login RPC';
    }

    /**
     * Get the method params
     *
     * @return array
     */
    public function getParams()
    {
        return [
            ['username', 'required'],
            ['password', 'required']
        ];
    }

    /**
     * Execute the server method
     */
    public function execute(Result $result, $username, $password)
    {
        // Find user
        $em   = $this->getContainer()->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:Users')->findOneBy(['username' => $username]);
        if ($user) {
            // Validate user
            $validator = $this->getContainer()->get('security.password_encoder');
            if ($validator->isPasswordValid($user, $password)) {
                // User login, save
                $token = new UsernamePasswordToken($user, $password, 'main', $user->getRoles());
                $this->getContainer()->get('security.token_storage')->setToken($token);
                $result->setResult(['status' => 'success', 'msg' => $this->trans('rpc.WorkspaceLogin.success')]);
            } else {
                $result->setResult(['status' => 'error', 'msg' => $this->trans('rpc.login.error')]);
            }
        } else {
            $result->setResult(['status' => 'error', 'msg' => $this->trans('rpc.login.error')]);
        }
    }
}