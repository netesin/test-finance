require(
	[
		'dojo',
		'dojo/on',
		'dojo/ready',
		'dojo/rpc/JsonService'
	], function (dojo, on, ready, JsonService) {

		var jsonRpcClient = new JsonService({'serviceUrl': '/api'});

		var api = function (method, params) {
			return jsonRpcClient.callRemote(method, params);
		};

		var login = function () {
			wait.show();
			dojo.byId('error').style.display = 'none';
			var username = dojo.byId('username').value;
			var password = dojo.byId('password').value;
			api('Login', {'username': username, 'password': password}).then(function (result) {
				if (result.status == 'success') {
					window.location.href = '/';
				} else {
					dojo.byId('error').innerText = result.msg;
					dojo.byId('error').style.display = '';
					wait.hide();
				}
			});
		};

		var wait = {
			show: function () {
				dojo.byId('wait').style.display = 'block';
			},
			hide: function () {
				dojo.byId('wait').style.display = 'none';
			}
		};

		ready(function () {
			wait.hide();
			on(dojo.byId('submit'), 'click', function () {
				login();
			});
			on(dojo.byId('form'), 'keydown', function (e) {
				if (e.keyCode == 13) {
					login();
				}
			});
		});
	});