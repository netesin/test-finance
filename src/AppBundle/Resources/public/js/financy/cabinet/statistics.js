require(
	[
		'dojo',
		'dijit/dijit',
		'dojo/date/locale',
		'dojo/on',
		'dojo/ready',
		'dojo/rpc/JsonService',
		'dojo/parser',
		'dojox/charting/Chart',
		'dojox/charting/axis2d/Default', 'dojox/charting/plot2d/Lines',
		'dojox/charting/themes/PlotKit/green',
		'dijit/form/DateTextBox'
	], function (dojo, dijit, locale, on, ready, JsonService, parser, Chart, Default, Lines, green) {

		var label = function (text, value, precision) {
			var date = new Date(value * 1000);
			return date.toDateString();
		};

        var wait = {
            show: function () {
                dojo.byId('wait').style.display = 'block';
            },
            hide: function () {
                dojo.byId('wait').style.display = 'none';
            }
        };

		var statisticsChart = new Chart('statistics-charting');
		statisticsChart.addPlot('statistics-charting', {type: Lines});
		statisticsChart.setTheme(green);
		statisticsChart.addAxis('x', {labelFunc: label, labelSizeChange: true});
		statisticsChart.addAxis('y', {vertical: true, fixUpper: "major", min: 0, labelSizeChange: true});

		var jsonRpcClient = new JsonService({'serviceUrl': '/api'});
		var api = function (method, params) {
			return jsonRpcClient.callRemote(method, params);
		};

		var renderChart = function () {
            wait.show();
			var from = locale.format(dijit.byId('dateFrom').get('value'), {datePattern: "yyyy-MM-dd", selector: "date"});
			var to = locale.format(dijit.byId('dateTo').get('value'), {datePattern: "yyyy-MM-dd", selector: "date"});
			api('StatisticsTime', {'from': from, 'to': to}).then(function (result) {
				statisticsChart.addSeries("statistics", result.data);
				statisticsChart.render();
                wait.hide();
			});
		};

		ready(function () {
			parser.parse();
			renderChart();
			on(dojo.byId('show'), 'click', function () {
				renderChart();
			});
		});
	});