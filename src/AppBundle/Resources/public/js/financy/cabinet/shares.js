require(
    [
        'dojo',
        'dojo/on',
        'dojo/query',
        'dojo/dom-construct',
        'dojo/ready',
        'dojo/rpc/JsonService'
    ], function (dojo, on, query, domConstruct, ready, JsonService) {

        var jsonRpcClient = new JsonService({'serviceUrl': '/api'});
        var api = function (method, params) {
            return jsonRpcClient.callRemote(method, params);
        };

        var initNode = function (node) {
            // Delete
            query('span.btn-delete', node).forEach(function (nodeBtn) {
                on(nodeBtn, 'click', function () {
                    wait.show();
                    api('SharesDelete', {'id': node.firstChild.innerText}).then(function () {
                        domConstruct.destroy(node);
                        wait.hide();
                    });
                });
            });
            // Update
            query('span.btn-update', node).forEach(function (nodeBtn) {
                on(nodeBtn, 'click', function () {
                    wait.show();
                    api('SharesUpdate', {'id': node.firstChild.innerText, 'count': node.children[2].firstChild.value}).then(function () {
                        wait.hide();
                    });
                });
            });
        };

        var wait = {
            show: function () {
                dojo.byId('wait').style.display = 'block';
            },
            hide: function () {
                dojo.byId('wait').style.display = 'none';
            }
        };

        ready(function () {
            // Add new shares to briefcase
            on(dojo.byId('add-button'), 'click', function () {
                wait.show();
                var share = dojo.byId('add-share').value;
                var count = dojo.byId('add-count').value;
                api('SharesAdd', {'share': share, 'count': count}).then(function (result) {
                    if (result.status == 'success') {
                        var html = '<div class="cabinet-shares-share"><span class="cabinet-shares-share-id">' + result.data.id + '</span><span class="cabinet-shares-share-share">' + result.data.share + '</span><span class="cabinet-shares-share-count"><input class="inp" value="' + result.data.count + '"/></span><span class="btn btn-update">' + result.msg.update + '</span><span class="btn btn-delete">' + result.msg.delete + '</span></div>';
                        var node = domConstruct.toDom(html);
                        domConstruct.place(node, dojo.byId('shares'), 'last');
                        // Init
                        initNode(node);
                    }
                    dojo.byId('add-share').value = 'YHOO';
                    dojo.byId('add-count').value = '1';
                    wait.hide();
                });
            });
            // Connect shares nodes
            query('div.cabinet-shares-share', dojo.byId('shares')).forEach(function (node) {
                initNode(node);
            });
        });
    });