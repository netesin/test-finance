Тестовое задание для PHP-разработчика

# Установка

Стандартная установка Symfony + console assets:install 

# Реализация

## Back-end:
Symfony 2.7, API для yahoo scheb/yahoo-finance-api, RPC JSON timiki/rpc-server-bundle

## Front-end:
DOJO JS, HTML, CSS

Для обмена клиент-сервер использован JSON RPC

# Затрачено времени

- Настройка среды (TeamCity, NGinx, Git)        : 15 мин
- Общая концепция приложения                    : 15-30 мин
- Написание основного кода (php, css, html)     : 5-6 ч
- Документаци                                   : 30-60 мин
- Общая доработка                               : 30 мин